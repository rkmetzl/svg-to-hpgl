use crate::lib::point::{Point};

pub fn cubic(p0: Point, p1: Point, p2: Point, p3: Point) -> Vec<Point> {
  let mut point_list: Vec<Point> = Vec::new();

  let n = 15;
  for i in 1..(n+1) {
    let q0 = subdivide_segment(p0, p1, i, n);
    let q1 = subdivide_segment(p1, p2, i, n);
    let q2 = subdivide_segment(p2, p3, i, n);

    let r0 = subdivide_segment(q0, q1, i, n);
    let r1 = subdivide_segment(q1, q2, i, n);

    let bezier_point = subdivide_segment(r0, r1, i, n);

    point_list.push(bezier_point);
  }

  point_list
}

pub fn quadratic(origin: Point, dest: Point, control: Point) -> Vec<Point> {
  let mut point_list: Vec<Point> = Vec::new();

  let n = 15;
  for i in 1..(n+1) {
    let origin_point = subdivide_segment(origin, control, i, n);
    let end_point    = subdivide_segment(control, dest, i, n);
    let bezier_point = subdivide_segment(origin_point, end_point, i, n);
    point_list.push(bezier_point);
  }

  point_list
}

pub fn reflect(control: &Point, end: &Point) -> Point {
  let diff_x = end.x - control.x;
  let diff_y = end.y - control.y;

  Point{x: end.x + diff_x, y: end.y + diff_y}
}

fn subdivide_segment(point_a: Point, point_b: Point, i: i32, n: i32) -> Point {
  let t: f64 = i as f64 / n as f64;
  let ax: f64 = point_a.x as f64;
  let ay: f64 = point_a.y as f64;
  let bx: f64 = point_b.x as f64;
  let by: f64 = point_b.y as f64;

  let x = ax + ((bx - ax) * t);
  let y = ay + ((by - ay) * t);

  Point{x: x, y: y}
}