use std::io::Error;
use xml::attribute::{OwnedAttribute};
#[path = "./utils.rs"] mod utils;

#[derive(Debug)]
pub struct Ellipse {
  pub cx: f64,
  pub cy: f64,
  pub rx: f64,
  pub ry: f64,
  pub stroke: String,
}

impl Ellipse {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Ellipse, Error> {
    let (mut cx, mut cy, mut rx, mut ry) = (0.0, 0.0, 0.0, 0.0);
    let mut stroke = String::new();

    for attr in attributes {
      match attr.name.local_name.as_str() {
        "cx" => cx = attr.value.parse::<f64>().unwrap_or(0.0),
        "cy" => cy = attr.value.parse::<f64>().unwrap_or(0.0),
        "rx" => rx = attr.value.parse::<f64>().unwrap_or(0.0),
        "ry" => ry = attr.value.parse::<f64>().unwrap_or(0.0),
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (
      Ellipse{ cx: cx, cy: cy, rx: rx, ry: ry, stroke: stroke }
    )
  }
}