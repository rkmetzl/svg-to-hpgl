use std::collections::{VecDeque};
use crate::lib::point::{Point};
use regex::Regex;

const X_PLOT_UNITS: u32 = 10365;
const Y_PLOT_UNITS: u32 = 7962;

pub fn autoscale(viewbox_val: &String) -> (f64, f64) {
  let viewbox_points = create_points(&viewbox_val, Point::default());

  // For now let's assume the first point is 0, 0
  let view_bounds: Point = viewbox_points[1];

  // The svg is wider than it is tall:
  if view_bounds.x > view_bounds.y {
    let d: f64 = X_PLOT_UNITS as f64 / view_bounds.x;
    let y_max = Y_PLOT_UNITS as f64 / d;

    (view_bounds.x, y_max)
  // The svg is taller than it is wide:
  } else {
    let d: f64 = Y_PLOT_UNITS as f64 / view_bounds.y;
    let x_max = X_PLOT_UNITS as f64 / d;

    (x_max, view_bounds.y)
  }
}

pub fn to_ellipse_points(x: f64, y: f64, rx: f64, ry: f64, step_size_deg: u32) -> Vec<Point> {
  let steps: u32 = 360 / step_size_deg;

  let mut points: Vec<Point> = Vec::new();

  for step in 0..=steps {
    let angle = (step * step_size_deg) as f64;

    let r: f64 = (rx * ry) / ( (ry * angle.to_radians().cos()).powf(2.0) + (rx * angle.to_radians().sin()).powf(2.0) ).sqrt();

    let px = r * angle.to_radians().cos();
    let py = r * angle.to_radians().sin();

    points.push(Point{x: x + px, y: y + py});
  }

  points
}

#[allow(dead_code)]
pub fn slice_alphanum(text: String) -> VecDeque<String> {
  let mut result: VecDeque<String> = VecDeque::new();

  let mut last = 0;
  for (index, matched) in text.match_indices(|c: char| c.is_alphabetic()) {
    if last != index {
      result.push_back(text[last..index].to_string());
    }
    result.push_back(matched.to_string());
    last = index + matched.len();
  }
  if last < text.len() {
    result.push_back(text[last..].to_string());
  }

  result
}

#[allow(dead_code)]
pub fn update_point_offset(coordinate_set: &Vec<Point>, new_offset: &Point, origin: &Point) -> Vec<Point> {
  let mut new_coords: Vec<Point> = Vec::new();
  for point in coordinate_set {
    new_coords.push((point.clone() - origin.clone()) + new_offset.clone());
  }

  new_coords
}

// Takes a string of coordinates (comma and/or whitespace delimited) and builds a Vector of Points.
// WARNING: Does not validate point string. Assumes an even number of points.
#[allow(dead_code)]
pub fn create_points(points: &String, origin: Point) -> Vec<Point> {
  // take points string, split by " "
  // take each unit and split by "," or " ", inserting into new Point
  // Add point to list of points
  let mut point_list = Vec::new();

  let mut fixed_points = str::replace(points, "-", " -");

  let trailing_decimals = Regex::new(r"\.(?P<dd>\d+)\.").unwrap();

  fixed_points = trailing_decimals.replace_all(&fixed_points, ".$dd 0.").to_string();

  let re = Regex::new(r"(\s+|,)").unwrap();
  let mut split_points: Vec<_> = re.split(&fixed_points).into_iter().collect();
  split_points.retain(|&i| i  != "");

  let point_pairs: Vec<&[_]> = split_points.chunks(2).collect();

  for pair in point_pairs {
    let x = pair[0].parse::<f64>().unwrap_or(0.0);
    let y = pair[1].parse::<f64>().unwrap_or(0.0);

    point_list.push(Point{ x: x + origin.x, y: y + origin.y });
  }

  point_list
}
