use std::io::Error;
use xml::attribute::{OwnedAttribute};
use std::collections::{VecDeque};

#[path = "./utils.rs"] mod utils;
#[path = "./bezier.rs"] mod bezier;

use crate::lib::point::{Point};
use crate::lib::path_polyline::{PathPolyline};


#[derive(Debug)]
pub struct Path {
  d: String,
  stroke: String,
}

impl Path {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Path, Error> {
    let mut stroke = String::new();
    let mut d = String::new();

    for attr in attributes {
      match attr.name.local_name.as_str() {
        "d" => d = attr.value,
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (Path {
      d: d, stroke: stroke
    })
  }

  pub fn polylines(&self) -> Vec<PathPolyline> {
    let mut d_commands: VecDeque<String> = utils::slice_alphanum(self.d.clone());
    let mut master_index = 0;
    let mut point_list: Vec<Vec<Point>> = Vec::new();
    let mut previous_point = Point::default();
    let mut prev_control_point = Point::default();

    // Iterate over indices, looking for a letter
    while d_commands.len() > 0 {
      let command = d_commands.pop_front().unwrap();

      let origin = {
        // If this is a (apparent) real command and it's lowercase:
        if command.len() == 1 && command == command.to_lowercase() {
          previous_point.clone()
        } else {
          Point::default()
        }
      };

      // Clear out any temporary points
      match command.to_lowercase().as_str() {
        "t" | "s" => {}
        _ => {
          prev_control_point = previous_point.clone();
        }
      }

      // H/h and V/v commands need to be handled separately because of their use of the previous
      // point.
      match command.as_str() {
        "h" => {
          let coordinates = d_commands.pop_front().unwrap();
          let coordinate = coordinates.trim().parse::<f64>().unwrap_or(0.0);

          let point = Point{ x: previous_point.x + coordinate, y: previous_point.y };

          previous_point = point.clone();
          point_list[master_index].push(point);
        }
        "H" => {
          let coordinates = d_commands.pop_front().unwrap();
          let coordinate = coordinates.trim().parse::<f64>().unwrap_or(0.0);

          let point = Point{ x: coordinate, y: previous_point.y };

          previous_point = point.clone();
          point_list[master_index].push(point);
        }
        "v" => {
          let coordinates = d_commands.pop_front().unwrap();
          let coordinate = coordinates.trim().parse::<f64>().unwrap_or(0.0);

          let point = Point{ x: previous_point.x, y: previous_point.y + coordinate };

          previous_point = point.clone();
          point_list[master_index].push(point);
        }
        "V" => {
          let coordinates = d_commands.pop_front().unwrap();
          let coordinate = coordinates.trim().parse::<f64>().unwrap_or(0.0);

          let point = Point{ x: previous_point.x, y: coordinate };

          previous_point = point.clone();
          point_list[master_index].push(point);
        }
        _ => {}
      }

      match command.to_lowercase().as_str() {
        "m" => {

          // Increment master index if there are currently points in the current index
          match point_list.get(master_index) {
            Some(_n) => {
              master_index += 1;
            }
            None => { }
          }

          let vec: Vec<Point> = Vec::new();
          point_list.push(vec);

          // Get string of coordinates:
          let coordinates = d_commands.pop_front().unwrap();
          let move_points: Vec<Point> = utils::create_points(&coordinates, origin);

          previous_point = *move_points.last().clone().unwrap();

          for point in move_points {
            point_list[master_index].push(point);
          }
        }
        "z" => {
          // Duplicate point at [master_index][0] and push to stack
          match point_list.get(master_index) {
            Some(n) => {
              match n.first() {
                Some(p) => {
                  let copy = p.clone();
                  point_list[master_index].push(copy);
                }
                None => {
                  panic!("Z command not added. Point list is empty.");
                }
              }
            }
            None => {
              panic!("Illegal Z command!");
            }
          }
        }
        "l" => {
          // Line command

          let coordinates = d_commands.pop_front().unwrap();
          let move_points: Vec<Point> = utils::create_points(&coordinates, origin);

          previous_point = *move_points.last().clone().unwrap();

          for point in move_points {
            point_list[master_index].push(point);
          }
        }
        "q" => {
          let coordinates = d_commands.pop_front().unwrap();
          let split_coordinates: Vec<Point> = utils::create_points(&coordinates, origin);
          let coordinate_sets: Vec<_> = split_coordinates.chunks(2).collect();

          let mut prev_end_point = Point::default();
          for (index, coordinate_set) in coordinate_sets.iter().enumerate() {
            let processed_coordinates = if index > 0 && command == command.to_lowercase() {
              utils::update_point_offset(&coordinate_set.to_vec(), &prev_end_point, &origin)
            } else {
              coordinate_set.to_vec()
            };

            let control_point = processed_coordinates[0];
            let end_point = processed_coordinates[1];

            prev_control_point = bezier::reflect(&control_point, &end_point);

            let move_points: Vec<Point> = bezier::quadratic(previous_point, end_point, control_point);

            previous_point = *move_points.last().clone().unwrap();
            prev_end_point = end_point.clone();

            for point in move_points {
              point_list[master_index].push(point);
            }
          }
        }
        "t" => {
          let coordinates = d_commands.pop_front().unwrap();
          let split_coordinates: Vec<Point> = utils::create_points(&coordinates, origin);

          for end_point in split_coordinates {
            // TODO: update point offset for sequentials
            let control_point = prev_control_point.clone();
            prev_control_point = bezier::reflect(&control_point, &end_point);

            let move_points: Vec<Point> = bezier::quadratic(previous_point, end_point, control_point);

            previous_point = *move_points.last().clone().unwrap();

            for point in move_points {
              point_list[master_index].push(point);
            }
          }
        }
        "c" => {
          let coordinates = d_commands.pop_front().unwrap();
          let split_coordinates: Vec<Point> = utils::create_points(&coordinates, origin);
          let coordinate_sets: Vec<_> = split_coordinates.chunks(3).collect();

          let mut prev_end_point = Point::default();
          for (index, coordinate_set) in coordinate_sets.iter().enumerate() {
            let processed_coordinates = if index > 0 && command == command.to_lowercase() {
              utils::update_point_offset(&coordinate_set.to_vec(), &prev_end_point, &origin)
            } else {
              coordinate_set.to_vec()
            };

            let p1 = processed_coordinates[0];
            let p2 = processed_coordinates[1];
            let p3 = processed_coordinates[2];

            // If the next command is an 's' command, save the control point for it to use
            prev_control_point = bezier::reflect(&p2, &p3);

            let move_points: Vec<Point> = bezier::cubic(previous_point, p1, p2, p3);

            previous_point = *move_points.last().clone().unwrap();
            prev_end_point = p3.clone();

            for point in move_points {
              point_list[master_index].push(point);
            }
          }
        }
        "s" => {
          let coordinates = d_commands.pop_front().unwrap();
          let split_coordinates: Vec<Point> = utils::create_points(&coordinates, origin);
          let coordinate_sets: Vec<_> = split_coordinates.chunks(2).collect();

          let mut prev_end_point = Point::default();
          for (index, coordinate_set) in coordinate_sets.iter().enumerate() {
            let processed_coordinates = if index > 0 && command == command.to_lowercase() {
              utils::update_point_offset(&coordinate_set.to_vec(), &prev_end_point, &origin)
            } else {
              coordinate_set.to_vec()
            };

            let p1 = prev_control_point.clone();
            let p2 = processed_coordinates[0];
            let p3 = processed_coordinates[1];

            prev_control_point = bezier::reflect(&p2, &p3);

            let move_points: Vec<Point> = bezier::cubic(previous_point, p1, p2, p3);

            previous_point = *move_points.last().clone().unwrap();
            prev_end_point = p3.clone();

            for point in move_points {
              point_list[master_index].push(point);
            }
          }
        }
        "a" => { /* TODO eventually: elliptical arc curve */ }
        _ => {}
      }
    }

    let mut polylist: Vec<PathPolyline> = Vec::new();
    for point_ary in point_list {
      let polyline = PathPolyline { points: point_ary, stroke: self.stroke.clone() };
      polylist.push(polyline);
    }

    polylist
  }
}
