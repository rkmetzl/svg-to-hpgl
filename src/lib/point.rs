use std::ops::Add;
use std::ops::Sub;

#[derive(Debug, Copy, Clone)]
pub struct Point {
  pub x: f64,
  pub y: f64
}

impl Point {
  pub fn distance_from(&self, point: &Point) -> f64 {
    (point.x - self.x) / (point.y - self.y)
  }

  pub fn default() -> Point {
    Point{x: 0.0, y: 0.0}
  }
}

impl PartialEq for Point {
  fn eq(&self, other: &Self) -> bool {
    self.x == other.x && self.y == other.y
  }
}

impl Add for Point {
  type Output = Self;
  fn add(self, other: Self) -> Self {
    Self {
      x: self.x + other.x,
      y: self.y + other.y,
    }
  }
}

impl Sub for Point {
  type Output = Self;
  fn sub(self, other: Self) -> Self {
    Self {
      x: self.x - other.x,
      y: self.y - other.y,
    }
  }
}