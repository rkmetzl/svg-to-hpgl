use std::io::Error;
use xml::attribute::{OwnedAttribute};

pub struct Pen {
  pub color: String,
  pub position: u8,
}

impl Pen {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Pen, Error> {
    let mut position = 0;
    let mut color = String::new();
    // parse attributes
    for attr in attributes {
      match attr.name.local_name.as_str() {
        "color" => color = attr.value,
        "position" => position = attr.value.parse().unwrap_or(0),
        _ => {}
      }
    }

    Ok (
      Pen {color: color, position: position}
    )
  }
}