use std::io::Error;
use xml::attribute::{OwnedAttribute};

#[derive(Debug)]
pub struct Rect {
  pub x: f64,
  pub y: f64,
  pub w: f64,
  pub h: f64,
  pub fill: Option<String>,
  pub stroke: String,
}

impl Rect {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Rect, Error> {
    let (mut x, mut y, mut w, mut h) = (0.0, 0.0, 0.0, 0.0);
    let (mut fill, mut stroke) = ( String::new(), String::new() );
    // parse attributes
    for attr in attributes {
      match attr.name.local_name.as_str() {
        "x" => x = attr.value.parse::<f64>().unwrap_or(0.0),
        "y" => y = attr.value.parse::<f64>().unwrap_or(0.0),
        "width" => w = attr.value.parse::<f64>().unwrap_or(0.0),
        "height" => h = attr.value.parse::<f64>().unwrap_or(0.0),
        "fill" => fill = attr.value,
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (Rect {
      x: x, y: y, w: w, h: h, fill: Some(fill), stroke: stroke,
    })
  }
}
