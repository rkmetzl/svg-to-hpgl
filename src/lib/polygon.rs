use std::io::Error;
use xml::attribute::{OwnedAttribute};
#[path = "./utils.rs"] mod utils;

#[derive(Debug)]
pub struct Polygon {
  pub points: String,
  pub stroke: String,
}

impl Polygon {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Polygon, Error> {
    let mut points = String::new();
    let mut stroke = String::new();

    for attr in attributes {
      match attr.name.local_name.as_str() {
        "points" => points = attr.value,
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (
      Polygon { points: points, stroke: stroke}
    )
  }
}
