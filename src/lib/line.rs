use std::io::Error;
use xml::attribute::{OwnedAttribute};

#[derive(Debug)]
pub struct Line {
  pub x1: f64,
  pub y1: f64,
  pub x2: f64,
  pub y2: f64,
  pub stroke: String,
}

impl Line {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Line, Error> {
    let (mut x1, mut y1, mut x2, mut y2) = (0.0, 0.0, 0.0, 0.0);
    let mut stroke = String::new();
    // parse attributes
    for attr in attributes {
      match attr.name.local_name.as_str() {
        "x1" => x1 = attr.value.parse::<f64>().unwrap_or(0.0),
        "y1" => y1 = attr.value.parse::<f64>().unwrap_or(0.0),
        "x2" => x2 = attr.value.parse::<f64>().unwrap_or(0.0),
        "y2" => y2 = attr.value.parse::<f64>().unwrap_or(0.0),
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (
      Line { x1: x1, y1: y1, x2: x2, y2: y2, stroke: stroke}
    )
  }
}
