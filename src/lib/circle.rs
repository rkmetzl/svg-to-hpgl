use std::io::Error;
use xml::attribute::{OwnedAttribute};
#[path = "./utils.rs"] mod utils;

#[derive(Debug)]
pub struct Circle {
  pub cx: f64,
  pub cy: f64,
  pub r: f64,
  pub stroke: String,
}

impl Circle {
  pub fn from_attributes(attributes: Vec<OwnedAttribute>) -> Result<Circle, Error> {
    let (mut cx, mut cy, mut r) = (0.0, 0.0, 0.0);
    let mut stroke = String::new();

    for attr in attributes {
      match attr.name.local_name.as_str() {
        "cx" => cx = attr.value.parse::<f64>().unwrap_or(0.0),
        "cy" => cy = attr.value.parse::<f64>().unwrap_or(0.0),
        "r" => r = attr.value.parse::<f64>().unwrap_or(0.0),
        "stroke" => stroke = attr.value,
        _ => {}
      }
    }

    Ok (
      Circle{cx: cx, cy: cy, r: r, stroke: stroke}
    )
  }
}
