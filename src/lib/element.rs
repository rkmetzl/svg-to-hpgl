use crate::lib::point::Point;

use crate::lib::rect::Rect;
use crate::lib::circle::Circle;
use crate::lib::line::Line;
use crate::lib::ellipse::Ellipse;
use crate::lib::polyline::Polyline;
use crate::lib::path_polyline::PathPolyline;
use crate::lib::polygon::Polygon;

#[path = "./utils.rs"] mod utils;

pub trait Element {
  fn point_list(&self) -> Vec<Point>;
  fn stroke(&self) -> &String;
}


impl Element for Circle {
  fn point_list(&self) -> Vec<Point> {
    utils::to_ellipse_points(self.cx, self.cy, self.r, self.r, 5)
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for Ellipse {
  fn point_list(&self) -> Vec<Point> {
    utils::to_ellipse_points(self.cx, self.cy, self.rx, self.ry, 5)
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for Line {
  fn point_list(&self) -> Vec<Point> {
    vec![
      Point{x: self.x1, y: self.y1},
      Point{x: self.x2, y: self.y2}
    ]
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for PathPolyline {
  fn point_list(&self) -> Vec<Point> {
    self.points.clone()
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for Polygon {
  fn point_list(&self) -> Vec<Point> {
    let mut polyline: Vec<Point> = utils::create_points(&self.points, Point::default());
    let first = polyline[0].clone();
    polyline.push(first);
    polyline
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for Polyline {
  fn point_list(&self) -> Vec<Point> {
    utils::create_points(&self.points, Point::default())
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}

impl Element for Rect {
  fn point_list(&self) -> Vec<Point> {

    vec![
      Point{x: self.x, y: self.y},
      Point{x: self.x + self.w, y: self.y},
      Point{x: self.x + self.w, y: self.y + self.h},
      Point{x: self.x, y: self.y + self.h},
      Point{x: self.x, y: self.y},
    ]
  }

  fn stroke(&self) -> &String {
    &self.stroke
  }
}
