extern crate xml;

use std::fs::File;
use std::io::BufReader;
use clap::{Arg,App};
use xml::reader::{EventReader, XmlEvent};
use std::collections::VecDeque;

use std::io::Write;

pub mod lib;
use lib::element;

use lib::rect;
use lib::line;
use lib::polyline;
use lib::polygon;
use lib::circle;
use lib::ellipse;
use lib::path;

use lib::point::Point;

#[path = "./lib/utils.rs"] mod utils;

use lib::pen;

fn main() {
  let args = App::new("svg-to-hpgl")
    .version("0.1.0")
    .about("SVG to HPGL converter")
    .author("Ryan Metzler")
    .args(&[
      Arg::new("input")
        .about("SVG file to read")
        .short('i')
        .long("input")
        .takes_value(true),
      Arg::new("output")
        .about("File to write")
        .short('o')
        .long("output")
        .takes_value(true),
    ]).get_matches();

  let input_path = args.value_of("input").unwrap();
  let output_path = args.value_of("output").unwrap();

  let file = File::open(input_path).unwrap();
  let file = BufReader::new(file);

  let mut elements: Vec<Box<dyn element::Element>> = Vec::new();
  let mut pens = Vec::new();
  let mut viewbox_val = String::new();

  let parser = EventReader::new(file);
  for e in parser {
    match e {
      Ok(XmlEvent::StartElement { name, attributes, .. }) => {
        match name.local_name.as_str() {
          "svg" => {
            for attr in attributes {
              match attr.name.local_name.as_str() {
                "viewBox" => {
                  viewbox_val = attr.value.clone();
                }
                _ => {}
              }
            }
            continue;
          }
          "rect" => {
            let rect = rect::Rect::from_attributes(attributes).unwrap();
            elements.push(Box::new(rect));
          }
          "line" => {
            let line = line::Line::from_attributes(attributes).unwrap();
            elements.push(Box::new(line));
          }
          "polyline" => {
            let polyline = polyline::Polyline::from_attributes(attributes).unwrap();
            elements.push(Box::new(polyline));
          }
          "polygon" => {
            let polygon = polygon::Polygon::from_attributes(attributes).unwrap();
            elements.push(Box::new(polygon));
          }
          "circle" => {
            let circle = circle::Circle::from_attributes(attributes).unwrap();
            elements.push(Box::new(circle));
          }
          "ellipse" => {
            let ellipse = ellipse::Ellipse::from_attributes(attributes).unwrap();
            elements.push(Box::new(ellipse));
          }
          "path" => {
            let path = path::Path::from_attributes(attributes).unwrap();
            for polyline in path.polylines() {
              elements.push(Box::new(polyline));
            }
          }
          "pen" => {
            let pen = pen::Pen::from_attributes(attributes).unwrap();
            pens.push(pen);
          }
          _ => {}
        }
      }
      Err(e) => {
        println!("Error: {}", e);
        break;
      }
      _ => {
      }
    }
  }

  // For now, viewbox_val is necessary, but ideally we can use the svg's height and width and/or
  // set to default scaling if none is given:
  let (x_max, y_max) = utils::autoscale(&viewbox_val);

  // Output is modifiable, and will be updated as we traverse pens and the point_lists they contain
  let mut output = format!("IN;VS3;SC0,{:.2},0,{:.2};", x_max, y_max).to_string();

  for pen in pens {
    let mut point_list_collection: Vec<VecDeque<Point>> = Vec::new();
    for element in elements.iter().filter(|e| e.stroke() == &pen.color) {
      let point_list: VecDeque<_> = element.point_list().into();
      point_list_collection.push(point_list);
    }

    if point_list_collection.len() < 1 { continue }

    let pen_select = format!("SP{};", pen.position);
    output.push_str(&pen_select);

    // TODO: sort point_list_collection here

    for mut point_list in point_list_collection {
      // Transform point_list into hpgl command string, inverting y points as we do
      // append output with string

      let first_point = point_list[0].clone();
      point_list.pop_front();
      let mut hpgl_sequence = format!("PU{:.2},{:.2};", first_point.x, y_max - first_point.y);
      for point in point_list {
        let next_command = format!("PD{:.2},{:.2};", point.x, y_max - point.y);
        hpgl_sequence.push_str(&next_command);
      }

      output.push_str(&hpgl_sequence);
    }
  }

  output.push_str("SP0;");
  let mut file = std::fs::File::create(output_path).expect("create failed");
  file.write_all(output.as_bytes()).expect("write failed");
}
